import { Router } from "express";
import * as studentsController from "./students.controller";
import controllerWrapper from "../application/utilites/controller-wrapper";
import validator from "../application/middlewares/validation.middleware";
import { studentCreateSchema, studentUpdateSchema } from "./students.schema";
import { idParamSchema } from "../application/schemas/id-param.schema";
import uploadMiddleware from "../application/middlewares/upload.middlewares";

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   get:
 *     summary: Get all students
 *     tags: [Students]
 *     parameters:
 *        - in: query
 *          name: name
 *          schema:
 *            type: string
 *          required: false
 *          description: Student name
 *     responses:
 *       200:
 *         description: The list of students.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Student'
 *       404:
 *         description: Students with this name was not found
 *       500:
 *         description: Some server error
 *
 */
router.get("/", controllerWrapper(studentsController.getAllStudents));

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   get:
 *     summary: Get student by id
 *     tags: [Students]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Student id
 *     responses:
 *       200:
 *         description: The student by id
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Student'
 *       400:
 *         description: Validation falied
 *       404:
 *         description: Student with this id was not found
 *       500:
 *         description: Some server error
 *
 */
router.get(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentByid)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   post:
 *     summary: Create student
 *     tags: [Students]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/StudentCreate'
 *     responses:
 *       201:
 *         description: The student was created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/StudentGetAfterCreateOrSettingPhoto'
 *       400:
 *         description: Validation falied / Student with this email already exists
 *       500:
 *         description: Some server error
 *
 */
router.post(
  "/",
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   patch:
 *     summary: Update student by id
 *     tags: [Students]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Student id
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/StudentUpdate'
 *     responses:
 *       200:
 *         description: Student updated.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Student'
 *       400:
 *         description: Validation falied
 *       404:
 *         description: Student with this id not found
 *       500:
 *         description: Some server error
 *
 */
router.patch(
  "/:id",
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentByid)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/group:
 *   patch:
 *     summary: Update group for student by id
 *     tags: [Students]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Student id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/SetGroupForStudent'
 *     responses:
 *       200:
 *         description: Group set for student
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Student'
 *       400:
 *         description: Validation falied
 *       404:
 *         description: This student does not exists
 *       500:
 *         description: Some server error
 *
 */
router.patch(
  "/:id/group",
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.setGroupForStudent)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/image:
 *   patch:
 *     summary: Set image for student by id
 *     tags: [Students]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Student id
 *     requestBody:
 *       required: true
 *       content:
 *          multipart/form-data:
 *           schema:
 *             $ref: '#/components/schemas/SetImageForStudent'
 *     responses:
 *       200:
 *         description: Image for student set
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/StudentGetAfterCreateOrSettingPhoto'
 *       400:
 *         description: Validation falied / File is not provided
 *       404:
 *         description: Student with this id not found
 *       500:
 *         description: Some server error
 *
 */
router.patch(
  "/:id/image",
  validator.params(idParamSchema),
  uploadMiddleware.single("file"),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.addStudentImage)
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   delete:
 *     summary: Remove student by id
 *     tags: [Students]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Student id
 *     responses:
 *       204:
 *         description: Object deleted
 *       400:
 *         description: Validation falied
 *       404:
 *         description: Student with this id was not found
 *       500:
 *         description: Some server error
 *
 */
router.delete(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(studentsController.deleteStudentById)
);

export default router;
