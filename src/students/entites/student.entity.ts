import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { CoreEntity } from "../../application/enteties/core.entety";
import { Group } from "../../groups/enteties/group.entity";
import { Mark } from "../../marks/entites/mark.entity";

@Entity({ name: "students" })
export class Student extends CoreEntity {
  @Column({
    type: "varchar",
    nullable: false,
  })
  @Index()
  name: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  surname: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  email: string;

  @Column({
    type: "numeric",
    nullable: true,
  })
  age: number;

  @Column({
    type: "varchar",
    nullable: true,
  })
  imagePath: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: false,
    eager: false,
    onDelete: "SET NULL",
  })
  @JoinColumn({ name: "group_id" })
  group: Group;

  @Column({
    type: "integer",
    nullable: true,
    name: "group_id",
  })
  groupId: number | null;

  @OneToMany(() => Mark, (mark) => mark.student)
  marks: Mark[];
}
