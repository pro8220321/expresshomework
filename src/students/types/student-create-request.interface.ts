import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { IStudent } from "./student.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentCreate:
 *       type: object
 *       required:
 *         - name
 *         - surname
 *         - age
 *         - email
 *       properties:
 *         name:
 *           type: string
 *           description: The name of student
 *         surname:
 *           type: string
 *           description: The surname of student
 *         age:
 *           type: number
 *           nullable: true
 *           description: The age of student
 *         email:
 *           type: string
 *           description: The email of student
 *       example:
 *         name: michael
 *         surname: scott
 *         age: 21
 *         email: dundermifflin@apple.com
 */

export interface IstudentCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<IStudent, "id">;
}
