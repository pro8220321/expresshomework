import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { IStudent } from "./student.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentUpdate:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The name of student
 *         surname:
 *           type: string
 *           description: The surname of student
 *         email:
 *           type: string
 *           description: The email of student
 *         age:
 *           type: number
 *           description: The age of student
 *       example:
 *         name: Bill
 *         surname: Gates
 *         email: billgates@outlook.com
 *         age: 22
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     SetGroupForStudent:
 *       type: object
 *       properties:
 *         groupId:
 *           type: string
 *           description: The group id of student
 *       example:
 *         groupId: 2
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     SetImageForStudent:
 *       type: object
 *       properties:
 *         file:
 *           type: file
 *           description: The group id of student
 *       example:
 *         file: icon-user.jpg
 */

export interface IstudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudent>;
}
