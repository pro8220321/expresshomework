import { IStudent } from "./student.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     Student:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - groupName
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the student
 *         name:
 *           type: string
 *           description: The name of student
 *         surname:
 *           type: string
 *           description: The surname of student
 *         email:
 *           type: string
 *           description: The email of student
 *         age:
 *           type: ['string','null']
 *           description: The age of student
 *         groupName:
 *           type:  ['string','null']
 *           description: The student group name
 *       example:
 *         id: 1
 *         name: Bill
 *         surname: Gates
 *         email: billgates@outlook.com
 *         age: 22
 *         groupName: Microsoft
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentGetAfterCreateOrSettingPhoto:
 *       type: object
 *       required:
 *         - name
 *         - surname
 *         - age
 *         - email
 *         - imagePath
 *         - groupId
 *         - id
 *         - createdAd
 *         - updateAd
 *       properties:
 *         name:
 *           type: string
 *           description: The name of student
 *         surname:
 *           type: string
 *           description: The surname of student
 *         age:
 *           type: ['string','null']
 *           description: The age of student
 *         email:
 *           type: string
 *           description: The email of student
 *         imagePath:
 *           type: ['string','null']
 *           description: The image path of student
 *         groupId:
 *           type: ['string','null']
 *           description: The group id for student
 *         id:
 *           type: string
 *           description: The auto-generated id of the student
 *         createdAd:
 *           type: string
 *           format: date
 *           description: The date the student was added
 *         updateAd:
 *           type: string
 *           format: date
 *           description: The date the student was updated
 *       example:
 *         name: michael
 *         surname: scott
 *         age: 21
 *         email: dundermifflin@apple.com
 *         imagePath: null
 *         groupId: null
 *         id: 5
 *         createdAd: 10:59:55.571506+00
 *         updateAd: 10:59:55.571506+00
 */

export interface IStudentGetResponse extends IStudent {
  createdAd: Date;
  updateAd: Date;
}
