import ObjectID from "bson-objectid";
import { HttpStatuses } from "../application/enums/http-statuses.enums";
import HttpException from "../application/exeptions/http-exeption";
import { IStudent } from "./types/student.interface";
import path from "path";
import fs from "fs/promises";
import { AppDataSource } from "../configs/database/data-source";
import { Student } from "./entites/student.entity";
import { DeleteResult, ILike, UpdateResult } from "typeorm";
import { Mark } from "../marks/entites/mark.entity";

const studentsReposetory = AppDataSource.getRepository(Student);
const marksReposetory = AppDataSource.getRepository(Mark);

async function findStudentById(id: string) {
  const student = await studentsReposetory
    .createQueryBuilder("student")
    .select([
      "student.id as id",
      "student.name as name",
      "student.surname as surname",
      "student.email as email",
      "student.age as age",
    ])
    .leftJoin("student.group", "group")
    .addSelect('group.name as "groupName"')
    .where("student.id = :id", { id })
    .getRawOne();
  return student;
}

export const getAllStudents = async (): Promise<Student[]> => {
  const students = await studentsReposetory
    .createQueryBuilder("student")
    .select([
      "student.id as id",
      "student.name as name",
      "student.surname as surname",
      "student.email as email",
      "student.age as age",
    ])
    .leftJoin("student.group", "group")
    .addSelect('group.name as "groupName"')
    .getRawMany();

  return students;
};

export const getStudentByid = async (id: string): Promise<Student> => {
  const student = await findStudentById(id);
  if (!student) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Student with this id was not found"
    );
  }

  return student;
};

export const createStudent = async (
  createStudentSchema: Omit<IStudent, "id">
): Promise<Student> => {
  const student = await studentsReposetory.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });
  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Student with this email already exists"
    );
  }

  return studentsReposetory.save(createStudentSchema);
};

export const updateStudentByid = async (
  id: string,
  updateStudentSchema: Partial<IStudent>
): Promise<Student> => {
  const findStudent = await findStudentById(id);

  if (!findStudent) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Student with this id not found"
    );
  }

  await studentsReposetory.update(id, updateStudentSchema);

  return findStudent;
};

export const setGroupForStudent = async (
  studentId: string,
  setGroupId: number
): Promise<Student> => {
  const student = await studentsReposetory
    .createQueryBuilder("studentgroup")
    .select(["*"])
    .where(`id = ${studentId}`)
    .getRawOne();

  if (!student) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "This student does not exists"
    );
  }

  await studentsReposetory
    .createQueryBuilder("studentgroup")
    .update()
    .set({ groupId: setGroupId })
    .where("id = :id", { id: studentId })
    .execute();

  const studentAsnwer = await studentsReposetory
    .createQueryBuilder("student")
    .select([
      "student.id as id",
      "student.name as name",
      "student.surname as surname",
      "student.email as email",
      "student.age as age",
    ])
    .leftJoin("student.group", "group")
    .addSelect('group.name as "groupName"')
    .where("student.id = :id", { id: studentId })
    .getRawOne();

  return studentAsnwer;
};

export const addStudentImage = async (id: string, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, "File is not provided");
  }
  const findStudent = await findStudentById(id);

  if (!findStudent) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Student with this id not found"
    );
  }

  try {
    const imageId = ObjectID().toHexString();
    const imageExtension = path.extname(filePath);
    const imageName = imageId + imageExtension;

    const studentsDirectoryName = "students";
    const currentPath = __dirname;
    const parentPath = path.dirname(currentPath);
    const studentsDirectoryPath = path.join(
      parentPath,
      "../src/",
      "public",
      studentsDirectoryName
    );
    const newImagePath = path.join(studentsDirectoryPath, imageName);

    await fs.rename(filePath, newImagePath);

    await studentsReposetory
      .createQueryBuilder("studentgroup")
      .update()
      .set({ imagePath: newImagePath })
      .where("id = :id", { id })
      .execute();

    const returnStudent = await studentsReposetory.findOne({
      where: {
        id: id,
      },
    });

    return returnStudent;
  } catch (error) {
    await fs.unlink(filePath);
    throw error;
  }
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
  const deleteStudentMarks = await marksReposetory
    .createQueryBuilder("studentMarks")
    .delete()
    .where("student_id = :id", { id })
    .execute();

  const student = await studentsReposetory.delete(id);

  if (!student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Student with this id was not found"
    );
  }

  return deleteStudentMarks;
};

export const getStudentsByName = async (
  name: string | undefined
): Promise<Student[]> => {
  const students = await studentsReposetory
    .createQueryBuilder("student")
    .select([
      "student.id as id",
      "student.name as name",
      "student.surname as surname",
      "student.email as email",
      "student.age as age",
    ])
    .leftJoin("student.group", "group")
    .addSelect('group.name as "groupName"')
    .where("student.name LIKE :name", { name })
    .getRawMany();

  if (students.length === 0) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Students with this name was not found"
    );
  }

  return students;
};
