import { Request, Response } from "express";
import * as studentsService from "./students.service";
import { ValidatedRequest } from "express-joi-validation";
import { IstudentUpdateRequest } from "./types/student-update-request.interface";
import { IstudentCreateRequest } from "./types/student-create-request.interface";
import { createResponse } from "../application/utilites/create.code.response";

export const getAllStudents = async (request: Request, response: Response) => {
  const name = request.query.name;
  let objectAnswer;
  if (!name) {
    const students = await studentsService.getAllStudents();
    objectAnswer = students;
  } else {
    const student = await studentsService.getStudentsByName(String(name));
    objectAnswer = student;
  }
  createResponse(request, response, objectAnswer);
};

export const getStudentByid = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const students = await studentsService.getStudentByid(id);
  createResponse(request, response, students);
};

export const createStudent = async (
  request: ValidatedRequest<IstudentCreateRequest>,
  response: Response
) => {
  const student = await studentsService.createStudent(request.body);
  createResponse(request, response, student);
};

export const updateStudentByid = async (
  request: ValidatedRequest<IstudentUpdateRequest>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentByid(id, request.body);
  createResponse(request, response, student);
};

export const setGroupForStudent = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.setGroupForStudent(
    id,
    request.body.groupId
  );
  createResponse(request, response, student);
};

export const addStudentImage = async (
  request: Request<{ id: string; file: Express.Multer.File }>,
  response: Response
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};
  const student = await studentsService.addStudentImage(id, path);
  createResponse(request, response, student);
};

export const deleteStudentById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  createResponse(request, response, student);
};
