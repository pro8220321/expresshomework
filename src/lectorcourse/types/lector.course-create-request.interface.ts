/**
 * @swagger
 * components:
 *   schemas:
 *     CreateLectorCourses:
 *       type: object
 *       required:
 *         - lectorId
 *         - courseId
 *       properties:
 *         lectorId:
 *           type: number
 *           description: The id of lector
 *         courseId:
 *           type: number
 *           description: The id of course
 *       example:
 *          lectorId: 4,
 *          courseId: 1
 */
