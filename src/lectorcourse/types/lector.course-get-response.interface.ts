/**
 * @swagger
 * components:
 *   schemas:
 *     LectorCourses:
 *       type: object
 *       required:
 *         - id
 *         - createdAd
 *         - updateAd
 *         - lectorId
 *         - courseId
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the lector course row
 *         createdAd:
 *           type: string
 *           format: date
 *           description: The date the lector course row was added
 *         updateAd:
 *           type: string
 *           format: date
 *           description: The date the lector course row was added
 *         lectorId:
 *           type: number
 *           description: The id of lector course row
 *         courseId:
 *           type: number
 *           description: The id of lector course row
 *       example:
 *          id: 28
 *          createdAd: 07:25:17.841001+00
 *          updateAd: 07:25:17.841001+00
 *          lectorId: 4
 *          courseId: 1
 */
