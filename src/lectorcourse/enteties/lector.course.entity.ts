import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
} from "typeorm";
import { CoreEntity } from "../../application/enteties/core.entety";
import { Lector } from "../../lectors/enteties/lector.entity";
import { Course } from "../../courses/enteties/course.entity";

@Entity({ name: "lector_courses" })
export class LectorCourse extends CoreEntity {
  @PrimaryColumn({ name: "lector_id" })
  lectorId: number;

  @PrimaryColumn({ name: "course_id" })
  courseId: number;

  @ManyToMany(() => Lector, (lector) => lector.courses)
  @JoinColumn({ name: "lector_id", referencedColumnName: "id" })
  lectors: Lector[];

  @ManyToOne(() => Course, (course) => course.lectors)
  @JoinColumn([{ name: "course_id", referencedColumnName: "id" }])
  courses: Course[];
}
