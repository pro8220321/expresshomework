import "dotenv/config";
import app from "./application/app";

const port = process.env.SERVER_PORT;

const startServer = async () => {
  app.listen(port, () => {
    console.log(`Listen to ${port} port`);
  });
};

startServer();
