import { Router } from "express";
import controllerWrapper from "../application/utilites/controller-wrapper";
import * as coursesController from "./courses.controller";
import { courseCreateSchema } from "./courses.schema";
import validator from "../application/middlewares/validation.middleware";
import { idParamSchema } from "../application/schemas/id-param.schema";

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses:
 *   get:
 *     summary: Get all courses
 *     tags: [Courses]
 *     responses:
 *       200:
 *         description: The list of courses
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Courses'
 *       500:
 *         description: Some server error
 */
router.get("/", controllerWrapper(coursesController.getCourses));

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The lectors managing API
 * /courses:
 *   post:
 *     summary: Create course
 *     tags: [Courses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CourseCreate'
 *     responses:
 *       201:
 *         description: Course was created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Courses'
 *       400:
 *         description: Validation falied
 *       500:
 *         description: Some server error
 *
 */
router.post(
  "/",
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse)
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}:
 *   patch:
 *     summary: Set lector for course
 *     tags: [Courses]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: number
 *          required: true
 *          description: Course id
 *        - in: query
 *          name: lector_id
 *          schema:
 *            type: number
 *          required: true
 *          description: Lector id
 *     responses:
 *       200:
 *         description: Lector added to course.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/LectorCourses'
 *       400:
 *         description: Can't add lector to course
 *       500:
 *         description: Some server error
 *
 */
router.patch(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(coursesController.addLector)
);

export default router;
