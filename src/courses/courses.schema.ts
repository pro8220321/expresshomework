import Joi from "joi";
import { Course } from "./enteties/course.entity";

export const courseCreateSchema = Joi.object<Omit<Course, "id">>({
  name: Joi.string().required(),
  description: Joi.string().required(),
  hours: Joi.number().required(),
});
