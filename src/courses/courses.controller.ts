import { Request, Response } from "express";
import * as courseServise from "./courses.service";
import { createResponse } from "../application/utilites/create.code.response";

export const getCourses = async (request: Request, response: Response) => {
  const lectorsId = request.query.lector_id;
  let objectAnswer;
  if (!lectorsId) {
    const allCourses = await courseServise.getAllCourses();
    objectAnswer = allCourses;
  } else {
    const lectorsCourses = await courseServise.getLectorsCourses(
      String(lectorsId)
    );
    objectAnswer = lectorsCourses;
  }
  createResponse(request, response, objectAnswer);
};

export const createCourse = async (request: Request, response: Response) => {
  const newCourse = await courseServise.createCourse(request.body);
  createResponse(request, response, newCourse);
};

export const addLector = async (request: Request, response: Response) => {
  const { id } = request.params;
  const lectorsId = String(request.query.lector_id);
  const addLector = await courseServise.addLector(id, lectorsId);
  createResponse(request, response, addLector);
};
