import { InsertResult } from "typeorm";
import { AppDataSource } from "../configs/database/data-source";
import { LectorCourse } from "../lectorcourse/enteties/lector.course.entity";
import { Course } from "./enteties/course.entity";
import HttpException from "../application/exeptions/http-exeption";
import { HttpStatuses } from "../application/enums/http-statuses.enums";
import { json } from "body-parser";

const coursesReposetory = AppDataSource.getRepository(Course);
const lectoreCoursesReposetory = AppDataSource.getRepository(LectorCourse);

export const getAllCourses = async (): Promise<Course[]> => {
  return coursesReposetory.find({});
};

export const createCourse = async (
  coursesCreateSchema: Omit<Course, "id">
): Promise<Course> => {
  return coursesReposetory.save(coursesCreateSchema);
};

export const addLector = async (
  idCourse: string,
  idLector: string
): Promise<LectorCourse> => {
  const addLectorToCourse = await lectoreCoursesReposetory
    .createQueryBuilder("lectorcourse")
    .insert()
    .values([{ lectorId: parseInt(idLector), courseId: parseInt(idCourse) }])
    .execute();

  const insertedEntity = await lectoreCoursesReposetory
    .createQueryBuilder()
    .orderBy("id", "DESC")
    .getOne();

  if (!insertedEntity) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Can't add lector to course"
    );
  }
  return insertedEntity;
};

export const getLectorsCourses = async (
  id: string
): Promise<LectorCourse[]> => {
  const lectorsCourses = await lectoreCoursesReposetory
    .createQueryBuilder("lectoreCourses")
    .leftJoin("lectoreCourses.courses", "courses")
    .addSelect('courses.name as "courses"')
    .where("lectoreCourses.lector_id = :id", { id })
    .select([
      'courses.name as "course"',
      'courses.description as "description"',
      'courses.hours as "hours"',
    ])
    .getRawMany();

  if (!lectorsCourses) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "There are no courses for this lecturer"
    );
  }

  return lectorsCourses;
};
