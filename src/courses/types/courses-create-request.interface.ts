/**
 * @swagger
 * components:
 *   schemas:
 *     CourseCreate:
 *       type: object
 *       required:
 *         - name
 *         - description
 *         - hours
 *       properties:
 *         name:
 *           type: string
 *           description: The name of the course
 *         description:
 *           type: string
 *           description: The description of the course
 *         hours:
 *           type: number
 *           description: The hours of the course
 *       example:
 *         name: music
 *         description: musical instruments usege
 *         hours: 9
 */
