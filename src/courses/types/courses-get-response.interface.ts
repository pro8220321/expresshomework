/**
 * @swagger
 * components:
 *   schemas:
 *     Courses:
 *       type: object
 *       required:
 *         - id
 *         - createdAd
 *         - updateAd
 *         - name
 *         - description
 *         - hours
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the course
 *         createdAd:
 *           type: string
 *           format: date
 *           description: The date the course was added
 *         updateAd:
 *           type: string
 *           format: date
 *           description: The date the course was added
 *         name:
 *           type: string
 *           description: The name of course
 *         description:
 *           type: string
 *           description: The description of course
 *         hours:
 *           type: number
 *           description: The hours of course
 *       example:
 *          id: 1
 *          createdAd: 11:13:51.882138+00
 *          updateAd: 11:13:51.882138+00
 *          name: music
 *          description: musical instruments skills
 *          hours: 9
 */
