import { HttpStatuses } from "../application/enums/http-statuses.enums";
import HttpException from "../application/exeptions/http-exeption";
import { AppDataSource } from "../configs/database/data-source";
import { Mark } from "./entites/mark.entity";

const marksReposetory = AppDataSource.getRepository(Mark);

export const addMark = async (marksCreateSchema: Omit<Mark, "id">) => {
  return marksReposetory.save(marksCreateSchema);
};

export const getAllStudentsMarks = async (id: string) => {
  const studentMarks = await marksReposetory
    .createQueryBuilder("marks")
    .leftJoin("marks.course", "course")
    .addSelect('course.name as "course_name"')
    .where("marks.student_id = :id", { id })
    .select(["marks.mark as mark", "course.name as course"])
    .getRawMany();

  if (!studentMarks) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Marks with this student was not found"
    );
  }

  return studentMarks;
};

export const getAllMarksOfCourse = async (id: string) => {
  const courseMarks = await marksReposetory
    .createQueryBuilder("marks")
    .leftJoin("marks.course", "course")
    .addSelect('course.name as "course"')
    .leftJoin("marks.lector", "lector")
    .addSelect('lector.name as "lector"')
    .leftJoin("marks.student", "student")
    .addSelect('student.name as "student"')
    .where("course.id = :id", { id })
    .select([
      "course.name as course",
      "lector.name as lector",
      "student.name as student",
      "marks.mark as mark",
    ])
    .getRawMany();

  return courseMarks;
};
