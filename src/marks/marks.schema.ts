import Joi from "joi";
import { Mark } from "./entites/mark.entity";

export const markCreateSchema = Joi.object<Omit<Mark, "id">>({
  mark: Joi.number().required(),
  course: Joi.number().required(),
  student: Joi.number().required(),
  lector: Joi.number().required(),
});
