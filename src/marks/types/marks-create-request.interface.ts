/**
 * @swagger
 * components:
 *   schemas:
 *    CreateMark:
 *       type: object
 *       required:
 *         - mark
 *         - course
 *         - student
 *         - lector
 *       properties:
 *         mark:
 *           type: string
 *           description: The mark of student
 *         course:
 *           type: string
 *           description: The course id
 *         student:
 *           type: string
 *           description: The student id
 *         lector:
 *           type: number
 *           description: The lector id
 *       example:
 *         mark: 5
 *         course: 1
 *         student: 1
 *         lector: 1
 */
