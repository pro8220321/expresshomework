/**
 * @swagger
 * components:
 *   schemas:
 *     Mark:
 *       type: object
 *       required:
 *         - id
 *         - student
 *         - course
 *       properties:
 *         course:
 *           type: object
 *           description: lector course
 *           items:
 *              $ref: '#/components/schemas/Courses'
 *         student:
 *           type: object
 *           description: lector student
 *           items:
 *              $ref: '#/components/schemas/StudentGetAfterCreateOrSettingPhoto'
 *         id:
 *           type: string
 *           description: The auto-generated id of the mark
 *       example:
 *          course: 1
 *          student: 1
 *          id: 5
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     NewMark:
 *       type: object
 *       required:
 *         - mark
 *         - course
 *         - student
 *         - lector
 *         - id
 *         - createdAd
 *         - updateAd
 *       properties:
 *         mark:
 *           type: number
 *           description: Mark of the student
 *         course:
 *           type: string
 *           description: Course id
 *         student:
 *           type: string
 *           description: Student id
 *         lector:
 *           type: string
 *           description: Lector id
 *         id:
 *           type: string
 *           description: The auto-generated id of the mark
 *         createdAd:
 *           type: string
 *           format: date
 *           description: The date the mark was created
 *         updateAd:
 *           type: string
 *           format: date
 *           description: The date the mark was updated
 *       example:
 *          mark: 5
 *          course: 1
 *          student: 1
 *          lector: 1
 *          id: 5
 *          createdAd: 15:26:57.793666+00
 *          updateAd: 15:26:57.793666+00
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentMarks:
 *       type: object
 *       required:
 *         - mark
 *         - course
 *       properties:
 *         mark:
 *           type: number
 *           description: Mark
 *         course:
 *           type: string
 *           description: Course
 *       example:
 *          mark: 5
 *          course: music
 *
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentMarksArr:
 *       type: array
 *       items:
 *          $ref: '#/components/schemas/StudentMarks'
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseMarks:
 *       type: object
 *       required:
 *         - course
 *         - lector
 *         - student
 *         - mark
 *       properties:
 *         course:
 *           type: string
 *           description: Name of the course
 *         lector:
 *           type: string
 *           description: Name of the lector
 *         student:
 *           type: string
 *           description: Name of the student
 *         mark:
 *           type: number
 *           description: Mark of the student
 *       examples:
 *          course: Music
 *          lector: Mozart
 *          student: Billy
 *          mark: 5
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseMarksArr:
 *       type: array
 *       items:
 *          $ref: '#/components/schemas/CourseMarks'
 */
