import { Router } from "express";
import controllerWrapper from "../application/utilites/controller-wrapper";
import * as marksController from "./marks.controller";
import validator from "../application/middlewares/validation.middleware";
import { markCreateSchema } from "./marks.schema";

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks:
 *   get:
 *     summary: Get marks
 *     tags: [Marks]
 *     parameters:
 *        - in: query
 *          name: student_id
 *          schema:
 *            type: number
 *          required: false
 *          description: student id
 *        - in: query
 *          name: course_id
 *          schema:
 *            type: number
 *          required: false
 *          description: course id
 *     responses:
 *       200:
 *         description: The list of marks.
 *         content:
 *           application/json:
 *             schema:
 *                anyOf:
 *                   - $ref: '#/components/schemas/CourseMarksArr'
 *                   - $ref: '#/components/schemas/StudentMarksArr'
 *       500:
 *         description: Some server error
 *
 */
router.get("/", controllerWrapper(marksController.getMarks));

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks:
 *   post:
 *     summary: Create mark for student
 *     tags: [Marks]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CreateMark'
 *     responses:
 *       201:
 *         description: Mark was added
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/NewMark'
 *       400:
 *         description: Validation falied
 *       500:
 *         description: Some server error
 *
 */
router.post(
  "/",
  validator.body(markCreateSchema),
  controllerWrapper(marksController.newMark)
);

export default router;
