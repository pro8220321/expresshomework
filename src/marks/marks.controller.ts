import { Request, Response } from "express";
import * as marksServise from "./marks.service";
import { createResponse } from "../application/utilites/create.code.response";

export const newMark = async (request: Request, response: Response) => {
  const createNewCourse = await marksServise.addMark(request.body);
  createResponse(request, response, createNewCourse);
};

export const getMarks = async (request: Request, response: Response) => {
  const studentId = request.query.student_id;
  const courseId = request.query.course_id;
  let objectAnswer;
  if (studentId !== undefined) {
    const studentMarks = await marksServise.getAllStudentsMarks(
      String(studentId)
    );
    objectAnswer = studentMarks;
  } else if (courseId !== undefined) {
    const coursesMarks = await marksServise.getAllMarksOfCourse(
      String(courseId)
    );
    objectAnswer = coursesMarks;
  }
  createResponse(request, response, objectAnswer);
};
