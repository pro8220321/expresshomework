import Joi from "joi";
import { Lector } from "./enteties/lector.entity";

export const lectorCreateSchema = Joi.object<Omit<Lector, "id">>({
  name: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().required(),
});
