import { AppDataSource } from "../configs/database/data-source";
import { Lector } from "./enteties/lector.entity";
import { HttpStatuses } from "../application/enums/http-statuses.enums";
import HttpException from "../application/exeptions/http-exeption";

const lectorsReposetory = AppDataSource.getRepository(Lector);

export const getAllLectors = async (): Promise<Lector[]> => {
  const lectors = await lectorsReposetory
    .createQueryBuilder("lector")
    .select([
      "lector.id as id",
      "lector.name as name",
      "lector.email as email",
      "lector.password as password",
    ])
    .getRawMany();

  return lectors;
};

export const getLectorById = async (id: string): Promise<Lector> => {
  const lector = await lectorsReposetory
    .createQueryBuilder("lectors")
    .distinctOn(["lectors.id", "course.id", "student.id"]) // get courses and students from marks and select only unique values
    .leftJoinAndSelect("lectors.marks", "mark")
    .leftJoinAndSelect("mark.student", "student")
    .leftJoinAndSelect("mark.course", "course")
    .where("lectors.id = :id", { id })
    .select([
      "lectors.id",
      "lectors.name",
      "lectors.email",
      "lectors.password",
      "mark.id",
      "student",
      "course",
    ])
    .getOne();

  if (lector === null) {
    throw new HttpException(HttpStatuses.NOT_FOUND, "Lector was not found");
  }

  return lector;
};

export const createLector = async (
  lectorCreateSchema: Omit<Lector, "id">
): Promise<Lector> => {
  return lectorsReposetory.save(lectorCreateSchema);
};
