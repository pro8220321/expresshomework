import { Request, Response } from "express";
import * as lectorServise from "./lectors.service";
import { createResponse } from "../application/utilites/create.code.response";

export const createLector = async (request: Request, response: Response) => {
  const newLector = await lectorServise.createLector(request.body);
  createResponse(request, response, newLector);
};

export const getAllLectors = async (request: Request, response: Response) => {
  const allLectors = await lectorServise.getAllLectors();
  createResponse(request, response, allLectors);
};

export const getLectorById = async (request: Request, response: Response) => {
  const { id } = request.params;
  const lector = await lectorServise.getLectorById(id);
  createResponse(request, response, lector);
};
