/**
 * @swagger
 * components:
 *   schemas:
 *     LectorCreate:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         name:
 *           type: string
 *           description: The name of lector
 *         email:
 *           type: string
 *           description: The email of lector
 *         password:
 *           type: string
 *           description: The age of lector
 *       example:
 *         name: Steven Hooking
 *         email: stevehooking@outlook.com
 *         password: password
 */
