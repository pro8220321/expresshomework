/**
 * @swagger
 * components:
 *   schemas:
 *     Lector:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - email
 *         - password
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the lector
 *         name:
 *           type: string
 *           description: The name of lector
 *         email:
 *           type: string
 *           description: The email of lector
 *         password:
 *           type: string
 *           description: The password of lector
 *       example:
 *           id: 1
 *           name: Steven Hooking
 *           email: teacherCreate@gmail.com
 *           password: teacherspassword
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorById:
 *       type: object
 *       required:
 *         - id
 *         - name
 *         - email
 *         - password
 *         - marks
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the student
 *         name:
 *           type: string
 *           description: The name of lector
 *         email:
 *           type: string
 *           description: The email of lector
 *         password:
 *           type: string
 *           description: The password of lector
 *         marks:
 *           type: array
 *           description: The students of group
 *           items:
 *              $ref: '#/components/schemas/Mark'
 *       example:
 *           id: 1,
 *           name: Steven Hooking
 *           email: teacherCreate@gmail.com
 *           password: teacherspassword
 *           marks: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorAfterCreate:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *         - id
 *         - createdAd
 *         - updateAd
 *       properties:
 *         name:
 *           type: string
 *           description: The name of lector
 *         email:
 *           type: string
 *           description: The email of lector
 *         password:
 *           type: string
 *           description: The password of lector
 *         id:
 *           type: string
 *           description: The auto-generated id of the lector
 *         createdAd:
 *           type: string
 *           format: date
 *           description: The date the lector was added
 *         updateAd:
 *           type: string
 *           format: date
 *           description: The date the lector was updated
 *       example:
 *           name: Steven Hooking
 *           email: teacherCreate@gmail.com
 *           password: teacherspassword
 *           id: 1
 *           createdAd: 10:52:05.372349+00
 *           updateAd: 10:52:05.372349+00
 */
