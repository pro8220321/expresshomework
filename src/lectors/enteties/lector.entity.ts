import { Column, Entity, JoinTable, ManyToMany, OneToMany } from "typeorm";
import { CoreEntity } from "../../application/enteties/core.entety";
import { Course } from "../../courses/enteties/course.entity";
import { Mark } from "../../marks/entites/mark.entity";

@Entity({ name: "lectors" })
export class Lector extends CoreEntity {
  @Column({
    type: "varchar",
    nullable: false,
  })
  name: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  email: string;

  @Column({
    type: "varchar",
    nullable: false,
  })
  password: string;

  @ManyToMany(() => Course, (course) => course.lectors)
  @JoinTable({
    name: "lector_courses",
    joinColumn: {
      name: "lector_id",
      referencedColumnName: "id",
    },
    inverseJoinColumn: {
      name: "course_id",
      referencedColumnName: "id",
    },
  })
  courses: Course[];

  @OneToMany(() => Mark, (mark) => mark.lector)
  marks: Mark[];
}
