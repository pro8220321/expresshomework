import { Router } from "express";
import controllerWrapper from "../application/utilites/controller-wrapper";
import * as lectorsController from "./lectors.controller";
import validator from "../application/middlewares/validation.middleware";
import { lectorCreateSchema } from "./lectors.schema";
import { idParamSchema } from "../application/schemas/id-param.schema";

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   get:
 *     summary: Get all lectors
 *     tags: [Lectors]
 *     responses:
 *       200:
 *         description: The list of lectors.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Lector'
 *       500:
 *         description: Some server error
 *
 */
router.get("/", controllerWrapper(lectorsController.getAllLectors));

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The students managing API
 * /lectors/{id}:
 *   get:
 *     summary: Get lector by id
 *     tags: [Lectors]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Lector id
 *     responses:
 *       200:
 *         description: The lector by id
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/LectorById'
 *       400:
 *         description: Validation falied
 *       404:
 *         description: Lector was not found
 *       500:
 *         description: Some server error
 */
router.get(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(lectorsController.getLectorById)
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   post:
 *     summary: Create lector
 *     tags: [Lectors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LectorCreate'
 *     responses:
 *       201:
 *         description: The list of lesctors
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/LectorAfterCreate'
 *       400:
 *         description: Validation falied
 *       500:
 *         description: Some server error
 *
 */
router.post(
  "/",
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector)
);

export default router;
