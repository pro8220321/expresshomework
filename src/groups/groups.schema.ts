import Joi from "joi";
import { Group } from "./types/group.interface";

export const groupCreateSchema = Joi.object<Omit<Group, "id">>({
  name: Joi.string().required(),
});

export const groupUpdateSchema = Joi.object<Partial<Group>>({
  name: Joi.string().optional(),
});
