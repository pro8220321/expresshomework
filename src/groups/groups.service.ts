import { DeleteResult } from "typeorm";
import { HttpStatuses } from "../application/enums/http-statuses.enums";
import HttpException from "../application/exeptions/http-exeption";
import { AppDataSource } from "../configs/database/data-source";
import { Group } from "./enteties/group.entity";

const groupsReposetory = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[]> => {
  const groups = await groupsReposetory
    .createQueryBuilder("group")
    .leftJoinAndSelect("group.students", "student")
    .getMany();

  return groups;
};

export const getStudentsByGroupId = async (id: string): Promise<Group[]> => {
  const group = await groupsReposetory
    .createQueryBuilder("group")
    .leftJoinAndSelect("group.students", "student")
    .where("group.id = :id", { id })
    .getMany();

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, "Group was not found");
  }

  return group;
};

export const createGroup = async (
  groupCreateSchema: Omit<Group, "id">
): Promise<Group> => {
  return groupsReposetory.save(groupCreateSchema);
};

export const updateGroupByid = async (
  id: string,
  updateStudentSchema: Partial<Group>
): Promise<Group> => {
  const group = await groupsReposetory.update(id, updateStudentSchema);

  const findGroup = await groupsReposetory
    .createQueryBuilder("group")
    .select([
      "group.name as name",
      "group.id as id",
      "group.createdAd as createdAd",
      "group.updateAd as updateAd",
    ])
    .where("id = :id", { id })
    .getRawOne();

  if (!findGroup) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      "Group with this id not found"
    );
  }

  return findGroup;
};

export const deleteGroupById = async (id: string): Promise<DeleteResult> => {
  const group = await groupsReposetory
    .createQueryBuilder("group")
    .leftJoinAndSelect("group.students", "student")
    .where("group.id = :id", { id })
    .getOne();

  if (!group) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      "Group with this id not found"
    );
  }

  const deletedGroup = await groupsReposetory.delete(id);

  return deletedGroup;
};
