import * as groupService from "./groups.service";
import { Request, Response } from "express";
import { createResponse } from "../application/utilites/create.code.response";

export const getGroupsWithStudents = async (
  request: Request,
  response: Response
) => {
  const groupswithStudents = await groupService.getAllGroups();
  createResponse(request, response, groupswithStudents);
};

export const getGroupWithStudents = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const groupwithStudents = await groupService.getStudentsByGroupId(
    request.params.id
  );
  createResponse(request, response, groupwithStudents);
};

export const createGroup = async (request: Request, response: Response) => {
  const newGroup = await groupService.createGroup(request.body);
  createResponse(request, response, newGroup);
};

export const updateGroupByid = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const group = await groupService.updateGroupByid(
    request.params.id,
    request.body
  );
  createResponse(request, response, group);
};

export const deleteGroupById = async (
  request: Request<{ id: string }>,
  response: Response
) => {
  const group = await groupService.deleteGroupById(request.params.id);
  createResponse(request, response, group);
};
