import { Router } from "express";
import controllerWrapper from "../application/utilites/controller-wrapper";
import * as groupsController from "./groups.controller";
import validator from "../application/middlewares/validation.middleware";
import { groupCreateSchema, groupUpdateSchema } from "./groups.schema";
import { idParamSchema } from "../application/schemas/id-param.schema";

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups:
 *   get:
 *     summary: Get all groups
 *     tags: [Groups]
 *     responses:
 *       200:
 *         description: The list of groups
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Group'
 *       500:
 *         description: Some server error
 */
router.get("/", controllerWrapper(groupsController.getGroupsWithStudents));

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   get:
 *     summary: Get group by id
 *     tags: [Groups]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Group id
 *     responses:
 *       200:
 *         description: The group by id
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Group'
 *       400:
 *         description: Validation falied
 *       404:
 *         description: Group was not found
 *       500:
 *         description: Some server error
 */
router.get(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(groupsController.getGroupWithStudents)
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups:
 *   post:
 *     summary: Create group
 *     tags: [Groups]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GroupCreate'
 *     responses:
 *       201:
 *         description: Group created
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/GroupAfterCreateOrUpdate'
 *       400:
 *         description: Validation falied
 *       500:
 *         description: Some server error
 */
router.post(
  "/",
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup)
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   patch:
 *     summary: Update group by id
 *     tags: [Groups]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Group id
 *     requestBody:
 *       required: false
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GroupUpdate'
 *     responses:
 *       200:
 *         description: Group was updated
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/GroupAfterCreateOrUpdate'
 *       400:
 *         description: Validation falied
 *       404:
 *         description: Group with this id not found
 *       500:
 *         description: Some server error
 */
router.patch(
  "/:id",
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupByid)
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   delete:
 *     summary: Remove group by id
 *     tags: [Groups]
 *     parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: Group id
 *     responses:
 *       204:
 *         description: The group was deleted
 *       400:
 *         description: Validation falied
 *       404:
 *         description: Group with this id not found
 *       500:
 *         description: Some server error
 */
router.delete(
  "/:id",
  validator.params(idParamSchema),
  controllerWrapper(groupsController.deleteGroupById)
);

export default router;
