import { IStudent } from "../../students/types/student.interface";

export interface Group {
  id: string;
  name: string;
}

export interface groupsResponse extends Group {
  students: IStudent[];
}
