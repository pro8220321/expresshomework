/**
 * @swagger
 * components:
 *   schemas:
 *     Group:
 *       type: object
 *       required:
 *         - id
 *         - createdAd
 *         - updateAd
 *         - name
 *         - students
 *       properties:
 *          id:
 *           type: string
 *           description: The auto-generated id of the group
 *          createdAd:
 *           type: string
 *           format: date
 *           description: The date the group was added
 *          updateAd:
 *           type: string
 *           format: date
 *           description: The date the group was added
 *          name:
 *           type: string
 *           description: The name of group
 *          students:
 *           type: array
 *           description: The students of group
 *           items:
 *              $ref: '#/components/schemas/StudentGetAfterCreateOrSettingPhoto'
 *       example:
 *          id: 2
 *          createdAd: 10:07:11.699767+00
 *          updateAd: 10:07:11.699767+00
 *          name: Microsoft
 *          students: []
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupAfterCreateOrUpdate:
 *       type: object
 *       required:
 *         - name
 *         - id
 *         - createdAd
 *         - updateAd
 *       properties:
 *          name:
 *           type: string
 *           description: The name of group
 *          id:
 *           type: string
 *           description: The auto-generated id of the group
 *          createdAd:
 *           type: string
 *           format: date
 *           description: The date the group was added
 *          updateAd:
 *           type: string
 *           format: date
 *           description: The date the group was added
 *       example:
 *          name: Facebook
 *          id: 5
 *          createdAd: 09:04:47.471009+00
 *          updateAd: 09:04:47.471009+00
 */
