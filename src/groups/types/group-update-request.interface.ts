import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { Group } from "./group.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupUpdate:
 *       type: object
 *       properties:
 *         name:
 *           type: string
 *           description: The name of group
 *       example:
 *         name: Apple
 */

export interface groupUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Group>;
}
