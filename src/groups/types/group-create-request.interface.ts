import { ContainerTypes, ValidatedRequestSchema } from "express-joi-validation";
import { Group } from "./group.interface";

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupCreate:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         name:
 *           type: string
 *           description: The name of group
 *       example:
 *         name: Microsoft
 */

export interface groupCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Group, "id">;
}
