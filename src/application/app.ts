import express from "express";
import cors from "cors";
import logger from "./middlewares/logger.middlewares";
import studentsRouter from "../students/students.router";
import groupsRouter from "../groups/groups.router";
import lectorsRouter from "../lectors/lectors.router";
import coursesRouter from "../courses/courses.router";
import marksRouter from "../marks/marks.router";
import bodyParser from "body-parser";
import exceptionsFilter from "./middlewares/exeptions.filter";
import path from "path";
import { AppDataSource } from "../configs/database/data-source";
import swaggerJsdoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);

const options = {
  definition: {
    openapi: "3.1.0",
    info: {
      title: "Univerity API",
      version: "0.1.0",
      description: "Univerity API",
      license: {
        name: "MIT",
        url: "https://spdx.org/licenses/MIT.html",
      },
    },
    servers: [
      {
        url: "http://localhost:3001/api/v1",
      },
    ],
  },
  apis: ["./**/*.router.ts", "./**/*.interface.ts"],
};

const specs = swaggerJsdoc(options);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs));

AppDataSource.initialize()
  .then(() => {
    console.log("TypeORM connected to datebase");
  })
  .catch((error) => {
    console.log("Error ", error);
  });

const staticFilesPath = path.join(__dirname, "../", "public");

app.use("/api/v1/public", express.static(staticFilesPath));
app.use("/api/v1/students", studentsRouter);
app.use("/api/v1/groups", groupsRouter);
app.use("/api/v1/lectors", lectorsRouter);
app.use("/api/v1/courses", coursesRouter);
app.use("/api/v1/marks", marksRouter);
app.use(exceptionsFilter);
export default app;
