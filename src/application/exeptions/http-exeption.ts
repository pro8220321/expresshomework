class HttpException extends Error {
  status: number;
  message: string;

  constructor(status: number, messege: string) {
    super(messege);
    this.status = status;
    this.message = messege;
  }
}

export default HttpException;
