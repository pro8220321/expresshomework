import { NextFunction, Request, Response } from "express";
import HttpException from "../exeptions/http-exeption";
import { HttpStatuses } from "../enums/http-statuses.enums";

const exceptionsFilter = (
  error: HttpException,
  request: Request,
  response: Response,
  next: NextFunction
) => {
  const status = error.status || HttpStatuses.INTERNAL_SERVER_ERROR;
  const message = error.message || "Something went wrong";
  response.status(status).send({ status, message });
  next();
};

export default exceptionsFilter;
