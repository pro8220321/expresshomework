import { BaseEntity, CreateDateColumn, PrimaryGeneratedColumn } from "typeorm";

export abstract class CoreEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: string;

  @CreateDateColumn({ type: "time with time zone", name: "created_at" })
  public createdAd: Date;

  @CreateDateColumn({ type: "time with time zone", name: "updated_at" })
  public updateAd: Date;
}
