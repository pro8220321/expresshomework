import { Response } from "express";

export const createResponse = (
  request: any,
  response: Response,
  object: any
) => {
  const method = request.method;
  if (method === "GET") {
    response.status(200);
    response.json(object);
  } else if (method === "POST") {
    response.status(201);
    response.json(object);
  } else if (method === "PATCH") {
    response.status(200);
    response.json(object);
  } else if (method === "DELETE") {
    response.status(204).send("Object deleted");
  }
};
